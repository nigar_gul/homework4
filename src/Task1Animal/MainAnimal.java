package Task1Animal;

public class MainAnimal {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.makeSound();
        Cat cat = new Cat();
        cat.makeSound();
        Cow cow = new Cow();
        cow.makeSound();
    }
}