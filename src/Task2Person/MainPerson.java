package Task2Person;

public class MainPerson {
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Nigar");
        student.setAge(27);
        student.setStudentID (8956);
        student.displayInfo();
        Teacher teacher = new Teacher();
        teacher.setName("Robert");
        teacher.setAge(35);
        teacher.setSubject("Mathematics");
        teacher.displayInfo();
    }
}
