package Task2Person;

public class Student extends Person{
    private int studentID;

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    @Override
    public void displayInfo() {
        System.out.println("Student's name is " +getName()+ " She is " +getAge()+ " years old. Her student ID is " +getStudentID()+"." );
    }
}
