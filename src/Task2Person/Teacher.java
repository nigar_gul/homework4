package Task2Person;

public class Teacher extends Person{
    private String Subject;

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    @Override
    public void displayInfo() {
        System.out.println("Teacher's name is " +getName()+ ". He is " +getAge()+ " years old. He teachs " +getSubject()+".");
    }
}
