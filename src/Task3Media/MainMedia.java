package Task3Media;

public class MainMedia {
    public static void main(String[] args) {
        Music music=new Music();
        music.setTitle("Amore");
        music.setDuration(3);
        music.play();
        Movie movie=new Movie();
        movie.setTitle("Life is beautiful");
        movie.setDuration(2.5);
        movie.play();
    }
}
