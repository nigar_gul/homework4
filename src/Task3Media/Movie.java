package Task3Media;

public class Movie extends Media{

    @Override
    public void play() {
        System.out.println("The movie -" + getTitle() + " is playing. Duration is " + getDuration()+ " hours.");
    }
}
