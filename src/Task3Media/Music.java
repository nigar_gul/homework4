package Task3Media;

public class Music extends Media{
    @Override
    public void play() {
        System.out.println("The music -" + getTitle() + " is playing. Duration is " + getDuration()+ " minutes.");
    }
}
