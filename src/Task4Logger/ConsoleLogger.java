package Task4Logger;

import java.util.SortedMap;

public class ConsoleLogger implements Logger {
    @Override
    public void logInfo() {
        System.out.println();
    }

    @Override
    public void logWarning() {

    }

    @Override
    public void logError() {

    }
}
