package Task4Logger;

public interface Logger {
    void logInfo();
    void logWarning();
    void logError();
}
