package Task4Logger;

public class MainLogger {
    public static void main(String[] args) {
        ConsoleLogger consoleLogger= new ConsoleLogger();
        consoleLogger.logInfo();
        consoleLogger.logError();
        consoleLogger.logWarning();
        FileLogger fileLogger=new FileLogger();
        fileLogger.logInfo();
        fileLogger.logError();
        fileLogger.logWarning();
    }
}
